# Thank you https://github.com/ barneygale/MCRcon and yamatt/MCRcon
import socket
import select
import struct
import re


class MinecraftRCON:

    def __init__(self, password, host="localhost", port=25575):
        self.__socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.__socket.connect((host, port))
        self._send(3, password)

    def close(self):
        self.__socket.close()

    def send(self, command):
        return self._send(2, command)

    def _send(self, out_type, out_data):
        # Send the data
        buff = struct.pack('<iii',
                           10 + len(out_data),
                           0,
                           out_type) + out_data + "\x00\x00"
        self.__socket.send(buff)

        #Receive a response
        in_data = ''
        ready = True
        while ready:
            #Receive an item
            tmp_len, tmp_req_id, tmp_type = struct.unpack('<iii', self.__socket.recv(12))
            tmp_data = self.__socket.recv(tmp_len - 8)  #-8 because we've already read the 2nd and 3rd integer fields

            #Error checking
            if tmp_data[-2:] != '\x00\x00':
                raise Exception('protocol failure', 'non-null pad bytes')
            tmp_data = tmp_data[:-2]

            #if tmp_type != out_type:
            #    raise Exception('protocol failure', 'type mis-match', tmp_type, out_type)

            if tmp_req_id == -1:
                raise Exception('auth failure')

            m = re.match('^Error executing: %s \((.*)\)$' % re.escape(out_data), tmp_data)
            if m:
                raise Exception('command failure', m.group(1))

            #Append
            in_data += tmp_data

            #Check if more data ready...
            ready = select.select([self.__socket], [], [], 0)[0]

        return in_data

