__author__ = 'The NetYeti'

from django.conf import settings

gettext = lambda s: s

MINECRAFT_STATUS_TEMPLATES = getattr(settings, 'MINECRAFT_STATUS_TEMPLATES', (
    ('minecraft/server_query.html', gettext('Full server status')),
    ('minecraft/players_list.html', gettext('Players List')),
    ('minecraft/plugins_list.html', gettext('Plugins List')),
    ('minecraft/server_status.html', gettext('Simple server status')),
))
MINECRAFT_RCON_TEMPLATES = getattr(settings, 'MINECRAFT_RCON_TEMPLATES', (
    ('minecraft/generic_rcon.html', gettext('Generic')),
))
