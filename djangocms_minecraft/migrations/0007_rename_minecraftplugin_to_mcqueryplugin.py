# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Renaming model 'MinecraftPlugin'
        db.rename_table(u'djangocms_minecraft_minecraftplugin', u'djangocms_minecraft_mcqueryplugin')
        if not db.dry_run:
            orm['ContentTypes.ContentType'].objects.filter(
                app_label='djangocms_minecraft', model='minecraftplugin').update(model='mcqueryplugin')

        # Adding model 'MCRCONPlugin'
        db.create_table(u'djangocms_minecraft_mcrconplugin', (
            (u'cmsplugin_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['cms.CMSPlugin'], unique=True, primary_key=True)),
            ('mchost', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['djangocms_minecraft.MCHost'])),
            ('template_path', self.gf('django.db.models.fields.CharField')(default=('minecraft/server_query.html', 'Full server status'), max_length=100)),
            ('command', self.gf('django.db.models.fields.CharField')(max_length=100)),
        ))
        db.send_create_signal(u'djangocms_minecraft', ['MCRCONPlugin'])

        # Adding field 'MCHost.rconpasswd'
        db.add_column(u'djangocms_minecraft_mchost', 'rconpasswd',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=80, blank=True),
                      keep_default=False)


    def backwards(self, orm):
        # Renaming model 'MinecraftPlugin'
        db.rename_table(u'djangocms_minecraft_mcqueryplugin', u'djangocms_minecraft_minecraftplugin')
        if not db.dry_run:
            orm['ContentTypes.ContentType'].objects.filter(
                app_label='djangocms_minecraft', model='mcqueryplugin').update(model='minecraftplugin')

        # Deleting model 'MCRCONPlugin'
        db.delete_table(u'djangocms_minecraft_mcrconplugin')

        # Deleting field 'MCHost.rconpasswd'
        db.delete_column(u'djangocms_minecraft_mchost', 'rconpasswd')


    models = {
        'cms.cmsplugin': {
            'Meta': {'object_name': 'CMSPlugin'},
            'changed_date': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'creation_date': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'language': ('django.db.models.fields.CharField', [], {'max_length': '15', 'db_index': 'True'}),
            'level': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'lft': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'parent': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cms.CMSPlugin']", 'null': 'True', 'blank': 'True'}),
            'placeholder': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cms.Placeholder']", 'null': 'True'}),
            'plugin_type': ('django.db.models.fields.CharField', [], {'max_length': '50', 'db_index': 'True'}),
            'position': ('django.db.models.fields.PositiveSmallIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'rght': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'tree_id': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'})
        },
        'cms.placeholder': {
            'Meta': {'object_name': 'Placeholder'},
            'default_width': ('django.db.models.fields.PositiveSmallIntegerField', [], {'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'slot': ('django.db.models.fields.CharField', [], {'max_length': '50', 'db_index': 'True'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'djangocms_minecraft.mchost': {
            'Meta': {'ordering': "['name']", 'object_name': 'MCHost'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'map_url': ('django.db.models.fields.CharField', [], {'default': "'http://www.thenetyeti.com:8123/?mapname=surface&zoom=8'", 'max_length': '150'}),
            'mchost': ('django.db.models.fields.CharField', [], {'default': "'127.0.0.1'", 'unique': 'True', 'max_length': '80'}),
            'mcport': ('django.db.models.fields.PositiveIntegerField', [], {'default': '25565'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '50'}),
            'rconpasswd': ('django.db.models.fields.CharField', [], {'max_length': '80', 'blank': 'True'}),
            'rconport': ('django.db.models.fields.PositiveIntegerField', [], {'default': '25575', 'blank': 'True'})
        },
        u'djangocms_minecraft.mcqueryplugin': {
            'Meta': {'object_name': 'MCQueryPlugin', '_ormbases': ['cms.CMSPlugin']},
            u'cmsplugin_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['cms.CMSPlugin']", 'unique': 'True', 'primary_key': 'True'}),
            'mchost': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['djangocms_minecraft.MCHost']"}),
            'template_path': ('django.db.models.fields.CharField', [], {'default': "('minecraft/server_query.html', 'Full server status')", 'max_length': '100'})
        },
        u'djangocms_minecraft.mcrconplugin': {
            'Meta': {'object_name': 'MCRCONPlugin', '_ormbases': ['cms.CMSPlugin']},
            u'cmsplugin_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['cms.CMSPlugin']", 'unique': 'True', 'primary_key': 'True'}),
            'command': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'mchost': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['djangocms_minecraft.MCHost']"}),
            'template_path': ('django.db.models.fields.CharField', [], {'default': "('minecraft/server_query.html', 'Full server status')", 'max_length': '100'})
        },
        u'djangocms_minecraft.minecraftserver': {
            'Meta': {'object_name': 'MinecraftServer', '_ormbases': ['cms.CMSPlugin']},
            u'cmsplugin_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['cms.CMSPlugin']", 'unique': 'True', 'primary_key': 'True'}),
            'map_url': ('django.db.models.fields.CharField', [], {'default': "'http://www.thenetyeti.com:8123/?mapname=surface&zoom=8'", 'max_length': '150'}),
            'mchost': ('django.db.models.fields.CharField', [], {'default': "'127.0.0.1'", 'max_length': '80'}),
            'mcport': ('django.db.models.fields.PositiveIntegerField', [], {'default': '25565'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'rconport': ('django.db.models.fields.PositiveIntegerField', [], {'default': '25575'})
        }
    }

    complete_apps = ['contenttypes', 'djangocms_minecraft']