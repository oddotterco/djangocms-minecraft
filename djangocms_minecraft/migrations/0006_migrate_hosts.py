# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import DataMigration
from django.db import models
from django.core.exceptions import ObjectDoesNotExist


class Migration(DataMigration):

    #no_dry_run = True

    def forwards(self, orm):
        """Create A unique list of hosts from MinecraftServer entries."""

        for host in orm.MinecraftServer.objects.all():

            try:
                mchost = orm.MCHost.objects.get(name=host.name)
            except ObjectDoesNotExist:
                new_host = orm.MCHost.objects.create()

                new_host.name = host.name
                new_host.mchost = host.mchost
                new_host.mcport = host.mcport
                new_host.rconport = host.rconport
                new_host.map_url = host.map_url
                new_host.save()

    def backwards(self, orm):
        """Backwards migration is not available."""
        pass

    models = {
        'cms.cmsplugin': {
            'Meta': {'object_name': 'CMSPlugin'},
            'changed_date': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'creation_date': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'language': ('django.db.models.fields.CharField', [], {'max_length': '15', 'db_index': 'True'}),
            'level': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'lft': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'parent': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cms.CMSPlugin']", 'null': 'True', 'blank': 'True'}),
            'placeholder': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cms.Placeholder']", 'null': 'True'}),
            'plugin_type': ('django.db.models.fields.CharField', [], {'max_length': '50', 'db_index': 'True'}),
            'position': ('django.db.models.fields.PositiveSmallIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'rght': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'tree_id': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'})
        },
        'cms.placeholder': {
            'Meta': {'object_name': 'Placeholder'},
            'default_width': ('django.db.models.fields.PositiveSmallIntegerField', [], {'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'slot': ('django.db.models.fields.CharField', [], {'max_length': '50', 'db_index': 'True'})
        },
        u'djangocms_minecraft.mchost': {
            'Meta': {'ordering': "['name']", 'object_name': 'MCHost'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'map_url': ('django.db.models.fields.CharField', [], {'default': "'http://www.thenetyeti.com:8123/?mapname=surface&zoom=8'", 'max_length': '150'}),
            'mchost': ('django.db.models.fields.CharField', [], {'default': "'127.0.0.1'", 'unique': 'True', 'max_length': '80'}),
            'mcport': ('django.db.models.fields.PositiveIntegerField', [], {'default': '25565'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '50'}),
            'rconport': ('django.db.models.fields.PositiveIntegerField', [], {'default': '25575'})
        },
        u'djangocms_minecraft.minecraftplugin': {
            'Meta': {'object_name': 'MinecraftPlugin', '_ormbases': ['cms.CMSPlugin']},
            u'cmsplugin_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['cms.CMSPlugin']", 'unique': 'True', 'primary_key': 'True'}),
            'mchost': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['djangocms_minecraft.MCHost']"}),
            'template_path': ('django.db.models.fields.CharField', [], {'default': "('minecraft/server_query.html', 'Full server status')", 'max_length': '100'})
        },
        u'djangocms_minecraft.minecraftserver': {
            'Meta': {'object_name': 'MinecraftServer', '_ormbases': ['cms.CMSPlugin']},
            u'cmsplugin_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['cms.CMSPlugin']", 'unique': 'True', 'primary_key': 'True'}),
            'map_url': ('django.db.models.fields.CharField', [], {'default': "'http://www.thenetyeti.com:8123/?mapname=surface&zoom=8'", 'max_length': '150'}),
            'mchost': ('django.db.models.fields.CharField', [], {'default': "'127.0.0.1'", 'max_length': '80'}),
            'mcport': ('django.db.models.fields.PositiveIntegerField', [], {'default': '25565'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'rconport': ('django.db.models.fields.PositiveIntegerField', [], {'default': '25575'})
        }
    }

    complete_apps = ['djangocms_minecraft']