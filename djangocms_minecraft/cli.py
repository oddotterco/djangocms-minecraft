#!/usr/bin/env python
# Directly copied from from https://github.com/Dinnerbone/mcstatus/

import socket
import sys
from pprint import pprint
from argparse import ArgumentParser

from query import MinecraftQuery
from mcrcon import MinecraftRCON


def main():
    parser = ArgumentParser(description="Query status of Minecraft multiplayer server",
                            epilog="Exit status: 0 if the server can be reached, otherwise nonzero."
                           )
    parser.add_argument("host", help="target hostname")
    parser.add_argument("-q", "--quiet", action='store_true', default=False,
                        help='don\'t print anything, just check if the server is running')
    parser.add_argument("-p", "--port", type=int, default=25565,
                        help='UDP port of server\'s "query" service [25565]')
    parser.add_argument("-r", "--retries", type=int, default=3,
                        help='retry query at most this number of times [3]')
    parser.add_argument("-t", "--timeout", type=int, default=10,
                        help='retry timeout in seconds [10]')
    parser.add_argument("-c", "--command", default='help',
                        help='command to execute (auto-selects RCON connection protocol)')
    parser.add_argument("-a", "--password", default=None,
                        help='password for RCON connection protocol')
    parser.add_argument("-P", "--rconport", type=int, default=25575,
                        help='TCP port of server\'s "rccon" service [25575]')

    options = parser.parse_args()

    if not options.command:
        try:
            query = MinecraftQuery(options.host, options.port,
                                   timeout=options.timeout,
                                   retries=options.retries)
            server_data = query.get_rules()
        except socket.error as e:
            if not options.quiet:
                print "socket exception caught:", e.message
                print "Server is down or unreachable."
            sys.exit(1)
    else:
        # RCON command
        try:
            cmd = MinecraftRCON(password=options.password, host=options.host, port=options.rconport)

            try:
                server_data = cmd.send(options.command)
            except Exception, e:
                print e
                sys.exit(0)

            cmd.close()

        except Exception, e:
            print e
            sys.exit(0)

    if not options.quiet:
        print "Server response data:"
        pprint(server_data.split("\n"))
    sys.exit(0)


if __name__=="__main__":
    main()
