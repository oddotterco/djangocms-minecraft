__author__ = 'The NetYeti'


from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool
from django.utils.translation import ugettext_lazy as _
from django.core.cache import cache
from models import MinecraftServer, MCQueryPlugin, MCRCONPlugin as MCRCONInstance
from djangocms_minecraft.query import MinecraftQuery
from djangocms_minecraft.mcrcon import MinecraftRCON
import socket
import settings


class MCStatusPlugin(CMSPluginBase):
    model = MCQueryPlugin
    render_template = settings.MINECRAFT_STATUS_TEMPLATES[0][0]
    name = _("Status and info")
    module = "Minecraft"
    admin_preview = True
    text_enabled = True

    def render(self, context, instance, placeholder):

        mchost = instance.mchost.mchost
        mcport = instance.mchost.mcport
        status = cache.get(mchost+"_full")

        if instance and instance.template_path:
            self.render_template = instance.template_path

        if not status:

            try:
                query = MinecraftQuery(host=mchost, port=mcport, timeout=5)
                status = query.get_rules()
            except socket.error as e:
                status = None
            else:
                cache.set(mchost+"_full", status)

        context.update({
            'status': status,
            'instance': instance,
        })

        return context

plugin_pool.register_plugin(MCStatusPlugin)


class MCRCONPlugin(CMSPluginBase):
    model = MCRCONInstance
    render_template = settings.MINECRAFT_RCON_TEMPLATES[0][0]
    name = _("Generic RCon")
    module = "Minecraft"
    admin_preview = True
    text_enabled = True

    def render(self, context, instance, placeholder):

        mchost = instance.mchost.mchost
        mcport = instance.mchost.rconport
        passwd = instance.mchost.rconpasswd
        status = cache.get(mchost+"_generic_rcon")

        if instance and instance.template_path:
            self.render_template = instance.template_path

        if not status:

            try:
                cmd = MinecraftRCON(password=passwd, host=mchost, port=mcport)

                try:
                    server_data = cmd.send(options.command)
                except Exception, e:
                    print e

                cmd.close()

            except Exception, e:
                print e
                status = None
            else:
                cache.set(mchost+"_generic_rcon", status)

        context.update({
            'status': status,
            'instance': instance,
        })

        return context

plugin_pool.register_plugin(MCRCONPlugin)




####################################################################################################################
# Deprecated tables and plugins - do not use these if you do not need to.  Please replace with the ones above
#
class MinecraftServerStatusPlugin(CMSPluginBase):
    """
    This class is deprecated in favor of the new MCStatusPlugin class.
    Please discontinue usage before the 1.0.0Final release of djangocms-minecraft - it will be removed at that point.
    """
    model = MinecraftServer
    render_template = "minecraft/server_status.html"
    name = _("(DEPRECATED) Status")
    module = "Minecraft"
    admin_preview = True
    text_enabled = True

    def render(self, context, instance, placeholder):

        mchost = instance.mchost
        mcport = instance.mcport
        status = cache.get(mchost+"_status")

        if not status:

            try:
                query = MinecraftQuery(host=mchost, port=mcport, timeout=5)
                status = query.get_status()
            except socket.error as e:
                status = None
            else:
                cache.set(mchost+"_status", status)

        context.update({
            'status': status,
            'instance': instance,
        })

        return context

plugin_pool.register_plugin(MinecraftServerStatusPlugin)


class MinecraftServerQueryPlugin(CMSPluginBase):
    """
    This class is deprecated in favor of the new MCStatusPlugin class.
    Please discontinue usage before the 1.0.0Final release of djangocms-minecraft - it will be removed at that point.
    """
    model = MinecraftServer
    render_template = "minecraft/server_query.html"
    name = _("(DEPRECATED) Query")
    module = "Minecraft"
    admin_preview = True
    text_enabled = True

    def render(self, context, instance, placeholder):

        mchost = instance.mchost
        mcport = instance.mcport
        status = cache.get(mchost+"_full")

        if not status:

            try:
                query = MinecraftQuery(host=mchost, port=mcport, timeout=5)
                status = query.get_rules()
            except socket.error as e:
                status = None
            else:
                cache.set(mchost+"_full", status)

        context.update({
            'status': status,
            'instance': instance,
        })

        return context

plugin_pool.register_plugin(MinecraftServerQueryPlugin)


class MinecraftPluginsListPlugin(CMSPluginBase):
    """
    This class is deprecated in favor of the new MCStatusPlugin class.
    Please discontinue usage before the 1.0.0Final release of djangocms-minecraft - it will be removed at that point.
    """
    model = MinecraftServer
    render_template = "minecraft/plugins_list.html"
    name = _("(DEPRECATED) Plugins")
    module = "Minecraft"
    admin_preview = True
    text_enabled = True
    cache = True

    def render(self, context, instance, placeholder):

        mchost = instance.mchost
        mcport = instance.mcport
        status = cache.get(mchost+"_full")

        if not status:

            try:
                query = MinecraftQuery(host=mchost, port=mcport, timeout=5)
                status = query.get_rules()
            except socket.error as e:
                status = None
            else:
                cache.set(mchost+"_full", status)

        context.update({
            'status': status,
            'instance': instance,
        })

        return context

plugin_pool.register_plugin(MinecraftPluginsListPlugin)
