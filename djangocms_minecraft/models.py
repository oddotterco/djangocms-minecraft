__author__ = 'The NetYeti'

from cms.models.pluginmodel import CMSPlugin
from django.db import models
from django.utils.translation import ugettext_lazy as _
import settings


# New class for re-usable server hosts used by plugins
class MCHost(models.Model):
    """
    Class used to conveniently store reusable Minecraft server information that the plugins will use to dynamically
    populate their output with.
    """
    name = models.CharField(_('Name'), help_text='Host name or server alias to use in the title of this plugin output',
                            max_length=50,
                            unique=True)
    mchost = models.CharField(_('Minecraft Host Address'),
                              help_text='IP address or hostname to access the Minecraft server via',
                              max_length=80,
                              unique=True,
                              default='127.0.0.1')
    mcport = models.PositiveIntegerField(_('Query Port'),
                                         help_text='Port to use for ping and query protocols.  Typically this is 25565 '
                                                   '(same as the Minecraft client connection port).', default=25565)
    rconport = models.PositiveIntegerField(_('RCON Port'),
                                           blank=True,
                                           help_text='The port to use for RCON access - not required, and not enabled '
                                                     'by default. (Typical port used for RCON is 25575)', default=25575)
    rconpasswd = models.CharField(_('RCON Password'),
                                  help_text='Password for access to this hosts RCON port',
                                  max_length=80,
                                  blank=True)
    map_url = models.CharField(_('Map URL'),
                               help_text='The url prefix for map links.  This can include zoom level, map type and more'
                                         ' if you are using something like Dynmap. It can also just be a static prefix'
                                         ' for static page sets where the map-name is appended to the end of the URL.',
                               max_length=150, default="http://www.thenetyeti.com:8123/?mapname=surface&zoom=8")

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name = _('host')
        verbose_name_plural = _('hosts')
        ordering = ["name"]


# Query plugin base
class MCQueryPlugin(CMSPlugin):
    """
    This class exposes the full query data from an mchost if it can be retrieved.  The data is then fed to the selected
    template.  Templates can be added or changed by adding a tuple setting to your project's settings.py file like so
    (this is the default)::

        MINECRAFT_STATUS_TEMPLATES =
        (
            ('minecraft/players_list.html', gettext('Players List')),
            ('minecraft/plugins_list.html', gettext('Plugins List')),
            ('minecraft/server_status.html', gettext('Simple server status')),
            ('minecraft/server_query.html', gettext('Full server status')),
        )
    """
    mchost = models.ForeignKey('MCHost')
    template_path = models.CharField(_("Template"), max_length=100, choices=settings.MINECRAFT_STATUS_TEMPLATES,
                                     default=settings.MINECRAFT_STATUS_TEMPLATES[0],
                                     help_text=_(
                                         'Enter a template (i.e. "minecraft/server_query.html") which will be rendered '
                                         'using the full query data-set.'))


# RCON plugin base
class MCRCONPlugin(CMSPlugin):
    """
    This class allows arbitrary execution of commands through the RCON protocol and feeds the output back to a template.
    Templates can be added or changed by adding a tuple setting to your project's settings.py file like so
    (this is the default)::

        MINECRAFT_RCON_TEMPLATES =
        (
            ('minecraft/generic_rcon.html', gettext('Generic')),
        )
    """
    mchost = models.ForeignKey('MCHost')
    template_path = models.CharField(_("Template"), max_length=100, choices=settings.MINECRAFT_RCON_TEMPLATES,
                                     default=settings.MINECRAFT_RCON_TEMPLATES[0],
                                     help_text=_(
                                         'Enter a template (i.e. "minecraft/server_query.html") which will be rendered '
                                         'using the full query data-set.'))
    command = models.CharField(_("Command"), max_length=100,
                               help_text=_('Command to be performed inside of the minecraft console connection.  '
                                           'Remember that this command will be executed without the player context, so '
                                           'beware.'))


# ####################################################################################################################
# Old tables - these are deprecated, but still used by original plugins for now
#

class MinecraftServer(CMSPlugin):
    """
    This class is deprecated in favor of the new MCQueryPlugin class.
    Please discontinue usage before the 1.0.0Final release of djangocms-minecraft - it will be removed at that point.
    """
    name = models.CharField(_('Name'), help_text='Host name or server alias to use in the title of this plugin output',
                            max_length=50)
    mchost = models.CharField(_('Minecraft Host Address'),
                              help_text='IP address or hostname to access the Minecraft server via',
                              max_length=80,
                              default='127.0.0.1')
    mcport = models.PositiveIntegerField(_('Query Port'),
                                         help_text='Port to use for ping and query protocols.  Typically this is 25565 '
                                                   '(same as the Minecraft client connection port).', default=25565)
    rconport = models.PositiveIntegerField(_('RCON Port'),
                                           help_text='The port to use for RCON access - not required, and not enabled '
                                                     'by default. (Typical port used for RCON is 25575)', default=25575)
    map_url = models.CharField(_('Map URL'),
                               help_text='The url prefix for map links.  This can include zoom level, map type and more'
                                         ' if you are using something like Dynmap. It can also just be a static prefix'
                                         ' for static page sets where the map-name is appended to the end of the URL.',
                               max_length=150, default="http://www.thenetyeti.com:8123/?mapname=surface&zoom=8")


